const loginButton = document.getElementById("login-button");
loginButton.addEventListener("click", function() {
  const correo = document.getElementById("correo").value.trim();
  const contraseña = document.getElementById("contraseña").value;

  // Verificar campos vacíos
  if (correo === "" || contraseña === "") {
    alert("Por favor ingrese su correo y contraseña.");
    return;
  }

  // Validar el correo electrónico
  const correoRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (!correoRegex.test(correo)) {
    alert("Por favor ingrese un correo electrónico válido.");
    return;
  }
});

